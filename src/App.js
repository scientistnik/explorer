import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Apis } from 'karmajs-ws';
import karmajs from 'karmajs';
import History from './History.js';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Modal from '@material-ui/core/Modal';
import Button from "@material-ui/core/Button";
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import axios from "axios";
import { Karma } from "./plugins"

axios.defaults.baseURL = "https://krmgw.karma.red/:3000";

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
  button: {
    "margin-left": 10
  }
});

const request = search => {
  return axios({
    method: 'POST',
    headers: {"Content-Type": "application/json"},
    data: `{"jsonrpc":"2.0","id":"1","method":"get_tx_state","params":["${search}"]}`
  })
}

class App extends Component {
  state = {
    operations: [],
    modal: false,
    msg: null,
    search: ''
  }

  constructor() {
    super();

    Karma.init()
    Karma.connect().then(async result => {
      window.account = await Karma.accounts["krm-eth"];
      this.setState({operations: await Karma.history.get_account_history(window.account.id, "1.11.0", 100, "1.11.0")});
    })
  }

  modalOpen(msg) {
    this.setState({modal: true, msg})
  }

  modalClose() {
    this.setState({modal: false})
  }

  setSearch(event) {
    this.setState({search: event.target.value})
  }

  async search() {
    let search = this.state.search;

    if (/^0x[\d\w]{64}$/.test(search) || /^1.11.\d*$/.test(search)) {
      let result = await request(search)
      let data = result.data.result[0];

      if (data)
        this.modalOpen(
          <div>
            <p><b>fromId:</b> { data.fromId }</p>
            <p><b>toId:</b> { (data.toId && data.toId.length == 66) ?
              <a href={`https://etherscan.io/tx/${data.toId}`} target="_blank">etherscan</a> : data.toId }</p>
            <p><b>Status:</b> { data.status }</p>
            <p><b>Last update:</b> { data.updatedAt }</p>
          </div>
        )
      /*
      window.web3.eth.getTransaction(search).then(result => {
        console.log(JSON.stringify(result))
      })
      */
    } else {
      try {
        let acc = await Karma.accounts[search];
        let ops = await Karma.history.get_account_history(acc.id, "1.11.0", 100, "1.11.0");

        let result = []
        for(let index in ops) {
          let op = ops[index]
          if ([op.op[1].from, op.op[1].to].includes(window.account.id))
            result.push(op)
        }
        //this.setState({operations: this.state.operations.filter(op => [op.op[1].from, op.op[1].to].includes(acc.id))})
        this.setState({operations: result})
        console.log(this.state.operations)
      } catch(error) {
        console.error(error)
      }
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to Karma</h1>
        </header>
        <form>
          <TextField
            id="name"
            label="Name / ID / Hash"
            value={this.state.search}
            onChange={this.setSearch.bind(this)}
          />
          <Button
            onClick={() => this.search()}
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Search
          </Button>
        </form>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Operation</TableCell>
              <TableCell>Time</TableCell>
              <TableCell>Direction</TableCell>
              <TableCell>Amount</TableCell>
              <TableCell>Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { this.state.operations.map( h =>
              <History key={h.id} element={h} request={request} modal={(msg) => this.modalOpen(msg)}/>)
            }
          </TableBody>
        </Table>
        <Modal open={this.state.modal}>
          <div style={ {top: "50%", left: "50%", transform: "translate(-50%, -50%)"} } className={classes.paper}>
            <div>{ this.state.msg }</div>
            <Button variant="contained" onClick={() => this.modalClose()} className="primary">Close</Button>
          </div>
        </Modal>
      </div>
    );
  }
}

const AppWrapped = withStyles(styles)(App);

export default AppWrapped;
