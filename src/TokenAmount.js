import React, { Component } from "react"
import { Karma } from "./plugins"


class TokenAmount extends Component {
  constructor() {
    super()

    this.state = {token: "Waiting..."}
  }

  componentDidMount() {
    Karma.db.get_assets([this.props.amount.asset_id]).then(result => {
      this.setState({token: result[0]})
    })
  }

  render() {
    return `${(this.props.amount.amount / 10 ** this.state.token.precision).toLocaleString()} ${this.state.token.symbol}`;
  }
}

export default TokenAmount;
