import React, { Component } from 'react';
import TokenAmount from "./TokenAmount.js";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Button from "@material-ui/core/Button";
import Web3 from "web3";
import { Karma } from "./plugins"

window.web3 = new Web3("https://mainnet.infura.io");

class History extends Component {
  state ={
    from: null,
    to: null,
    time: ''
  }

  componentDidMount() {
    let element = this.props.element,
        op = element.op[1];


    if (op.from === window.account.id) {
      this.getAccount(op.to).then(result => {
        this.setState({from: window.account, to: result});
      })
    } else {
      this.getAccount(op.from).then(result => {
        this.setState({from: result, to: window.account});
      })
    }

    Karma.db.get_block(element.block_num).then(block => {
      let period = Math.round((Date.now() - new Date(block.timestamp + 'Z')) / 1000 / 60),
          mins = Math.round(period % 60),
          hours = Math.round(((period - mins) / 60) % 24),
          days = Math.round((period - mins - hours * 60) / 60 / 24)

      period = days > 0 ? `${days} days ${hours} hrs ago` : `${hours > 0 ? `${hours} hrs ` : ''}${mins} mins ago`

      this.setState({time: `${period} (${block.timestamp})`})
    })
  }

  async getAccount(id) {
    return (await Karma.db.get_accounts([id]))[0];
  }

  checkOperation() {
    if (this.props.element.op[1].from === window.account.id) {
      this.props.modal(<div>Not found!</div>);
      return
    }

    this.props.request(this.props.element.id).then(result => {
      if (result.status == "200"){
        let data = result.data.result[0];

        if (data)
          this.props.modal(
            <div>
              <p><b>fromId:</b> { data.fromId }</p>
              <p><b>toId:</b> { (data.toId && data.toId.length == 66) ?
                <a href={`https://etherscan.io/tx/${data.toId}`} target="_blank">etherscan</a> : data.toId }</p>
              <p><b>Status:</b> { data.status }</p>
              <p><b>Last update:</b> { data.updatedAt }</p>
            </div>
          );
        else {
          this.props.modal(<div>Not found</div>)
        }
      }

    })
  }

  render() {
    return (
      <TableRow>
        <TableCell>
          <Button
            href={`https://explorer.karma.red/#/operations/operation/${window.account.id}%2F${this.props.element.id}`}
            target="_blank"
          >
            { this.props.element.id }
          </Button>
        </TableCell>
        <TableCell>{ this.props.element.op[0] === 0 && <p>Transfer</p> }</TableCell>
        <TableCell>{ this.state.time }</TableCell>
        <TableCell>{ this.state.from && this.state.from.name} &rarr; {this.state.to && this.state.to.name }</TableCell>
        <TableCell><TokenAmount amount={this.props.element.op[1].amount}/></TableCell>
        <TableCell>
          <Button variant="outlined" onClick={() => this.checkOperation()}>Check</Button>
        </TableCell>
      </TableRow>
    );
  }
}


export default History;
